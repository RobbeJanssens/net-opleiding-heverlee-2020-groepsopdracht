﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace dotNet_Core_Groepsopdracht.Models {
	public class NewsDeleteViewModel {
		public Guid Id { get; set; }

		[DisplayName("Titel")]
		public string Title { get; set; }

		[DisplayName("Datum aangemaakt")]
		public DateTime DateCreated { get; set; }

		//[DisplayName("Datum aangepast")]
		//public DateTime? DateAdjusted { get; set; }

		[DisplayName("Inhoud")]
		public string Content { get; set; }

		//[DisplayName("Afbeelding URL")]
		//public string ImageUrl { get; set; }

		[DisplayName("Auteur")]
		public string AuthorUserName { get; set; }

		//public Guid AuthorId { get; set; }


	}
}
