# About the project

The aim of our project is to create a user-friendly platform for small organisations, such as a PTA (Parent Teacher Association), a drama club or a small cultural centre, to showcase their upcoming events.

Small organisations are usually limited to advertising their events on their local municipality's website, posting them on Facebook, or distributing flyers, but often lack a central platform. This is something we wanted to remedy by giving organisations the opportunity to create a community! 

We also implemented the possibility to post news articles that encourage the community (registered users) to interact with each other by posting comments. A member of the community can go to the website, view upcoming events, book tickets, read news articles and comment on the articles. They can also contact the site administrators via a built-in contact form.

If a user creates an account on the website, he can also see an overview of the events for which he has reserved tickets.

On the administrator side, we have provided the ability to create different roles and the ability to assign very specific permissions to these roles. Users can then be assigned one or more of these roles.

In addition to the creation of roles, there are two other sections that are only accessible to users with the necessary authorisations: sponsor management and user management. Depending on an organisation's needs, specific permissions may be granted to board members as well as moderators. They will be able to create, edit or delete events, news items and user comments.

## About us

We are a group of 10 'newbie' developers who started studying .NET C# in April 2020. It's been quite the journey! For about seven months, we have had a really intensive training (all remote), where we went all the way from basic C# to a much more advanced knowledge of WinForms, WPF and .NET Core technologies. Along the way, we learnt a lot about SQL and Entity Framework, about HTML, CSS and Javascript and then some more.

This project is our final group-project built on the .NET Core platform. Over a period of 3 weeks, we have been working together using the Scrum framework. We have had daily standups where we briefly discussed our progress and any issues. Every Friday we held a sprint review where we reviewed the past week and determined the priorities for the coming week.

## Getting started

Please note that the back-end code is written in English and the UI in Dutch.

To get a local copy up and running, follow these simple steps:

### Prerequisites
- No third-party libraries or packages required
- Bootstrap v4.5.3 

### Installation
Clone or download the repository:
1. SSH: `git clone git@gitlab.com:c-ontwikkelaars-2020/dotnet-core-groepsopdracht.git`
2. HTTPS: `git clone https://gitlab.com/c-ontwikkelaars-2020/dotnet-core-groepsopdracht.git`

## Contribution

Contributions are what make the open source community such an amazing place to learn, inspire and create. Any contributions you make are greatly appreciated!

### First Time
1. [Fork](https://gitlab.com/c-ontwikkelaars-2020/dotnet-core-groepsopdracht/-/forks/new) the main (called `upstream`) repository
2. You now have your own copy of the repository, available at `https://gitlab.com/<namespace>/dotnet-core-groepsopdracht` where `<namespace>` is your namespace on GitLab
3. Clone your fork on your computer *(replace `<namespace>` by your namespace)*: `git clone https://gitlab.com/<namespace>/dotnet-core-groepsopdracht.git`
4. Add `upstream` as a remote called `upstream`: `git remote add upstream https://gitlab.com/<namespace>/very-hungry-penguins.git`

### On each merge request
1. Fetch new stuff from upstream: `git fetch upstream`
2. Make sure you're on your master branch: `git checkout master`
3. Update your master so it's in sync with upstream/master: `git rebase upsteam/master`
4. Create a new branch for your MR (replace `amazingfeature` with something... better ?): `git branch amazingfeature`
5. Switch to your new branch: `git checkout amazingfeature`
6. Write code, create commits etc.
7. Push your commits to a new branch on your fork: `git push --set-upstream origin amazingfeature`
8. Create your merge request using the correct button on GitLab.
9. Wait for the CI to be completed, if everything is going well, it's ready to merge. Otherwise, you can update the branch `amazingfeature` on your fork, the MR will be updated automatically.