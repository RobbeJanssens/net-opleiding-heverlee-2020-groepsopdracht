﻿using dotNet_Core_Groepsopdracht.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace dotNet_Core_Groepsopdracht.Models {

	public class RoleIndexViewModel {
		public Guid Id { get; set; }

		[DisplayName("Aantal users")]
		public int UserCount { get; set; }

		[DisplayName("Naam")]
		public string Name { get; set; }

		public Permissions Permissions { get; set; }
	}
}