﻿using System;

namespace dotNet_Core_Groepsopdracht.Models {
	public class UserDeleteViewModel {
		public Guid UserId { get; set; }
		public string UserName { get; set; }
	}
}
