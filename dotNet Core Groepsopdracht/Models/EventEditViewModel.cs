﻿using dotNet_Core_Groepsopdracht.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace dotNet_Core_Groepsopdracht.Models {

	public class EventEditViewModel {
		public Guid Id { get; set; }

		public List<PeriodViewModel> PeriodLijst { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = "Titel is vereist")]
		[DisplayName("Titel*")]
		[StringLength(100, ErrorMessage = "Titel is te lang")]
		public string Title { get; set; }

		[Required]
		[Range(0, int.MaxValue, ErrorMessage = "Maximum capaciteit kan niet lager dan 0 zijn.")]
		[DisplayName("Maximum capaciteit*")]
		public int MaxCap { get; set; }

		[Required]
		[Range(0, Int32.MaxValue, ErrorMessage = "Minimum capaciteit kan niet lager dan 0 zijn.")]
		[DisplayName("Minimum capaciteit*")]
		public int MinCap { get; set; }


		[Required(AllowEmptyStrings = false, ErrorMessage = "Beschrijving is vereist.")]
		[DisplayName("Beschrijving*")]
		[StringLength(500, ErrorMessage = "Beschrijving is te lang")]
		public string Description { get; set; }

		public string CurrentImgPath { get; set; }

		[DisplayName("Afbeelding")]
		public IFormFile Image { get; set; }

		// TODO: Datatype prijs aanpassen?!
		[DisplayName("Prijs")]
		public string Price { get; set; }



		public List<SelectListItem> Sponsortags { get; set; }
		public string[] SelectedSponsors { get; set; }

		[Required]
		[DisplayName("Straatnaam*")]
		[StringLength(100, ErrorMessage = "Straatnaam is te lang")]
		public string Street { get; set; }

		[Required]
		[Range(1, int.MaxValue, ErrorMessage = "Huisnummer kan niet kleiner zijn dan 1")]
		[DisplayName("Huisnummer*")]
		public int Number { get; set; }

		[DisplayName("Bus")]
		public string Bus { get; set; }

		[Required]
		[DisplayName("Postcode*")]
		public string ZipCode { get; set; }

		[Required]
		[DisplayName("Gemeente*")]
		[StringLength(100, ErrorMessage = "Naam is te lang")]
		public string City { get; set; }

		//Kijkt of startdatum vroeger is dat einddatum
		public bool PeriodIsCorrect(PeriodViewModel pvm) {
			if (pvm.Start <= pvm.End) {
				return true;
			}
			return false;
		}
		public EventEditViewModel( ) { }

		public EventEditViewModel(Sponsor sponsor) {
			SelectedSponsors = new string[sponsor.SponsorEvents.Count];
			for (int i = 0; i < sponsor.SponsorEvents.Count; i++) {
				SelectedSponsors[i] = sponsor.SponsorEvents[i].Sponsor.Naam + sponsor.SponsorEvents[i].Sponsor.Id;
			}
		}

	}
}