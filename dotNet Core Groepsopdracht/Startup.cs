using dotNet_Core_Groepsopdracht.Areas.Identity.Data;
using dotNet_Core_Groepsopdracht.Data.Database;
using dotNet_Core_Groepsopdracht.Database;
using dotNet_Core_Groepsopdracht.Entities;
using dotNet_Core_Groepsopdracht.Services;
using dotNet_Core_Groepsopdracht.Services.Design;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Localization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Globalization;

namespace dotNet_Core_Groepsopdracht {

	public class Startup {

		public Startup(IConfiguration configuration) {
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
			if (env.IsDevelopment( )) {
				app.UseDeveloperExceptionPage( );
				app.UseDatabaseErrorPage( );
			} else {
				app.UseExceptionHandler("/Home/Error");
				// The default HSTS value is 30 days. You may want to change this for production
				// scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts( );
			}
			CreateDatabase(app);

			app.UseStatusCodePages( );
			app.UseStatusCodePagesWithRedirects("/StatusCode?code={0}");
			app.UseHttpsRedirection( );

			// Added cultureinfo so that dates would always be displayed the 'European' way
			CultureInfo culture = CultureInfo.CreateSpecificCulture("nl-BE");
			DateTimeFormatInfo dateformat = new DateTimeFormatInfo {
				ShortDatePattern = "dd/MM/yyyy",
				LongDatePattern = "dd/MM/yyyy hh:mm"
			};
			culture.DateTimeFormat = dateformat;

			CultureInfo[] supportedCultures = new[] {
				culture
			};

			app.UseRequestLocalization(new RequestLocalizationOptions {
				DefaultRequestCulture = new RequestCulture(culture),
				SupportedCultures = supportedCultures,
				SupportedUICultures = supportedCultures,
			});

			app.UseStaticFiles( );

			app.UseRouting( );
			app.UseAuthentication( );
			app.UseAuthorization( );

			app.UseEndpoints(endpoints => {
				endpoints.MapControllerRoute(
					name: "default",
					pattern: "{controller=Home}/{action=Index}/{id?}");
				endpoints.MapRazorPages( );
			});
		}

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services) {
			services.AddControllersWithViews( );
			services.AddRazorPages( );
			services.AddDbContext<DatabaseContext>(options =>
				options.UseSqlServer(Configuration.GetConnectionString("LocalDB")));

			services.AddDefaultIdentity<User>(options => options.SignIn.RequireConfirmedAccount = true)
				.AddRoles<Role>( )
				.AddEntityFrameworkStores<DatabaseContext>( );

			// In memory implentation of IDatabase
			//services.AddSingleton<IDatabase, MemoryDatabase>( );

			services.AddScoped<IDatabase, Services.Database>( );

			services.AddTransient<IPhotoService, PhotoService>( );
		}

		private static void CreateDatabase(IApplicationBuilder app) {
			using IServiceScope serviceScope = app.ApplicationServices
				.GetRequiredService<IServiceScopeFactory>( )
				.CreateScope( );
			using DatabaseContext context = serviceScope.ServiceProvider.GetService<DatabaseContext>( );
			context.Database.EnsureDeleted( );
			if (context.Database.EnsureCreated( )) {
				SeedDatabase(context);
			}
		}

		private static void SeedDatabase(DatabaseContext context) {
			User user = new User();
			NewsItem newsItem = new NewsItem {Author = user, ImageUrl = "1a4beb49-d00c-4517-9835-6454a2d5d453_RodeDuivels.jpg"};

			context.Add(user);
			context.Add(newsItem);

			context.SaveChanges( );
		}
	}
}