﻿function AddListItemListener(idBtn, url, idContainer, func) {
    $(`#${idBtn}`).on("click", () => {
        $.ajax({
            async: true,
            data: $('#form').serialize(),
            type: "POST",
            url: url,
            success: function (partialView) {
                console.log($('#form').serialize());
                $(`#${idContainer}`).html(partialView);
                func();
            }
        });
    });
}

function AddPeriodItemListener(idBtn, url, idContainer) {    
    AddListItemListener(idBtn, url, idContainer, () => {
        $(".datepicker").datetimepicker({
            format: 'DD/MM/YYYY HH:mm'
        });
    });    
}