﻿using System;

namespace dotNet_Core_Groepsopdracht.Models {
	public class AddressViewModel {
		public Guid Id { get; set; }

		public string Street { get; set; }

		public int Number { get; set; }

		public string Bus { get; set; }

		public string ZipCode { get; set; }

		public string City { get; set; }
	}
}
