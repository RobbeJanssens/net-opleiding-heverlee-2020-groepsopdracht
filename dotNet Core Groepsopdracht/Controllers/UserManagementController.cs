﻿using dotNet_Core_Groepsopdracht.Areas.Identity.Data;
using dotNet_Core_Groepsopdracht.Attributes;
using dotNet_Core_Groepsopdracht.Data.Database;
using dotNet_Core_Groepsopdracht.Entities;
using dotNet_Core_Groepsopdracht.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;

namespace dotNet_Core_Groepsopdracht.Controllers {

	[PermissionAuthorize(Permissions.RoleManagement | Permissions.UserManagement, PermissionAuthorizeType.HasAny)]
	public class UserManagementController : Controller {

		//
		// Fields
		//
		private readonly IDatabase _database;

		//
		// Constructors
		//
		public UserManagementController(IDatabase database) {
			_database = database;
		}

		//
		// Methods
		//
		[HttpGet]
		public IActionResult IndexAsync(string sortUser, string searchString, string currentFilter, int? pageNumber) {

			ViewData["NameSortParm"] = String.IsNullOrEmpty(sortUser) ? "name_desc" : "";
			ViewData["DateSortParm"] = sortUser == "Date" ? "date_desc" : "Date";
			ViewData["CurrentFilter"] = searchString;
			ViewData["CurrentSort"] = sortUser;

			var users = _database.GetUsers();

			//add pagination
			if (searchString != null) {
				pageNumber = 1;
			} else {
				searchString = currentFilter;
			}

			//checks if the user has fields containing the search string
			if (!String.IsNullOrEmpty(searchString)) {
				users = users.Where(u => u.LastName.Contains(searchString) || u.FirstName.Contains(searchString) || u.Email.Contains(searchString));
			}

			//sorts the list
			users = sortUser switch
			{
				"name_desc" => users.OrderByDescending(u => u.LastName),
				"date_desc" => users.OrderByDescending(u => u.CreatedOn),
				"Date" => users.OrderBy(u => u.CreatedOn),
				_ => users.OrderBy(u => u.LastName)
			};

			//some more page code
			//int pageSize = 3;
			//return View(await PaginatedList<User>.CreateAsync(users, pageNumber ?? 1, pageSize));

			return View(users.Select(u => new UserListViewModel(u)).Take(100));
		}

		[HttpPost]
		public IActionResult Index(List<UserListViewModel> userListViewModels) {
			List<Role> roles = _database.GetRoles( ).ToList( );
			List<User> users = _database.GetUsers( ).ToList( );
			foreach (UserListViewModel u in userListViewModels) {
				User user = users.FirstOrDefault(x => x.UserId == u.UserId);
				List<UserRole> userRoles = new List<UserRole>();
				foreach (string rolename in u.SelectedRoles) {
					UserRole userRole = new UserRole();
					//userRole.UserId = user.UserId;
					userRole.User = user;
					Role role = roles.FirstOrDefault(x => x.Name == rolename);
					//userRole.RoleId = role.Id;
					userRole.Role = role;
					userRoles.Add(userRole);
				}
				user.UserRoles = userRoles;
				_database.UpdateUser(user, user.UserId);
			}
			return View(new List<UserListViewModel> { });
		}

		[HttpGet]
		[PermissionAuthorize(Permissions.UserManagement)]
		public IActionResult UserDeleteConfirm(Guid id) {
			User userToDelete = _database.FindUserById(id);
			if (userToDelete == null) return new NotFoundResult( );

			UserDeleteViewModel userDeleteViewModel = new UserDeleteViewModel()
			{
				UserId = userToDelete.Id,
				UserName = userToDelete.UserName
			};
			return View(userDeleteViewModel);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		[PermissionAuthorize(Permissions.UserManagement)]
		public IActionResult UserDelete(Guid id) {
			User userToDelete = _database.FindUserById(id);
			if (userToDelete == null) return new NotFoundResult( );
			_database.RemoveUser(userToDelete);
			return RedirectToAction(nameof(Index));
		}

		[PermissionAuthorize(Permissions.RoleManagement)]
		public IActionResult RoleOverview( ) {
			//adres:   /UserManagement/RoleOverview

			//omzetten van Role naar RoleIndexViewModel
			List<RoleIndexViewModel> roleIndexViewModelList = new List<RoleIndexViewModel>();
			foreach (Entities.Role role in _database.GetRoles( )) {
				roleIndexViewModelList.Add(
					new RoleIndexViewModel {
						Id = role.Id,
						Name = role.Name,
						Permissions = role.Permissions,
						UserCount = role.UserRoles is null ? 0 : role.UserRoles.Count
					}
				);
			}

			return View(roleIndexViewModelList);
		}

		[HttpGet]
		[PermissionAuthorize(Permissions.RoleManagement)]
		public IActionResult CreateRole( ) {
			return View( );
		}

		[HttpPost]
		[PermissionAuthorize(Permissions.RoleManagement)]
		public IActionResult CreateRole([FromForm] RoleCreateViewModel viewModel) {
			if (!TryValidateModel(viewModel))
				return View(viewModel);
			Permissions permissions = new Permissions();
			if (viewModel.RoleManagement) permissions |= Permissions.RoleManagement;
			if (viewModel.UserManagement) permissions |= Permissions.UserManagement;
			if (viewModel.CreateNewsItem) permissions |= Permissions.CreateNewsItem;
			if (viewModel.EditNewsItem) permissions |= Permissions.EditNewsItem;
			if (viewModel.ApproveNewsItem) permissions |= Permissions.ApproveNewsItem;
			if (viewModel.EventManagement) permissions |= Permissions.EventManagement;
			if (viewModel.DeleteReaction) permissions |= Permissions.DeleteReaction;
			if (viewModel.ViewStatistics) permissions |= Permissions.ViewStatistics;
			if (viewModel.SponsorManagement) permissions |= Permissions.SponsorManagement;


			_database.AddRole(new Role {
				Id = Guid.NewGuid( ),
				Name = viewModel.Name,
				Permissions = permissions
			});

			return RedirectToAction(nameof(RoleOverview)); // Seems more logical after creating a role to go back to the overview of the roles
		}

		[HttpGet]
		[PermissionAuthorize(Permissions.RoleManagement)]
		public IActionResult EditRole(Guid id) {
			Role role = _database.FindRoleById(id);
			if (role is null) {
				return new NotFoundResult( );
			}
			RoleEditViewModel viewModel = new RoleEditViewModel{
				Id = id,
				Name = role.Name,
				Permissions = role.Permissions
			};

			return View(viewModel);
		}

		[HttpPost]
		[PermissionAuthorize(Permissions.RoleManagement)]
		public IActionResult EditRole(Guid id, [FromForm] RoleEditViewModel viewModel) {
			if (!TryValidateModel(viewModel)) return View(viewModel);

			Role role = _database.FindRoleById(id);
			if (role == null) return View(viewModel);

			Permissions permissions = new Permissions();
			if (viewModel.RoleManagement) permissions |= Permissions.RoleManagement;
			if (viewModel.UserManagement) permissions |= Permissions.UserManagement;
			if (viewModel.CreateNewsItem) permissions |= Permissions.CreateNewsItem;
			if (viewModel.EditNewsItem) permissions |= Permissions.EditNewsItem;
			if (viewModel.ApproveNewsItem) permissions |= Permissions.ApproveNewsItem;
			if (viewModel.EventManagement) permissions |= Permissions.EventManagement;
			if (viewModel.DeleteReaction) permissions |= Permissions.DeleteReaction;
			if (viewModel.ViewStatistics) permissions |= Permissions.ViewStatistics;
			if (viewModel.SponsorManagement) permissions |= Permissions.SponsorManagement;

			role.Name = viewModel.Name;
			role.Permissions = permissions;

			_database.UpdateRole(role, id); // Add try catch?
											// Moeten we hier new {ID = id} niet meegeven? Waarom niet? Het werkt precies zonder ...
			return RedirectToAction(nameof(RoleOverview)); // Seems more logical after re-naming a role to go back to the overview of the roles
		}

		[HttpGet]
		[PermissionAuthorize(Permissions.RoleManagement)]
		//[ValidateAntiForgeryToken] // TODO / WARNING: When antiforgerytoken is enabled, delete does not work! => HTTP ERROR 400
		public IActionResult DeleteRoleConfirm(Guid id) {
			Role roleToDelete = _database.FindRoleById(id);
			if (roleToDelete == null) return new NotFoundResult( );

			RoleDeleteViewModel roleDeleteViewModel = new RoleDeleteViewModel()
			{
				Id = roleToDelete.Id,
				Name = roleToDelete.Name,
				//Permissions = roleToDelete.Permissions
			};
			return View(roleDeleteViewModel);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		[PermissionAuthorize(Permissions.RoleManagement)]
		public IActionResult DeleteRole(Guid id) {
			Role roleToDelete = _database.FindRoleById(id);
			if (roleToDelete == null) return new NotFoundResult( );
			_database.RemoveRole(roleToDelete);
			return RedirectToAction(nameof(RoleOverview));
		}

		public IActionResult RoleDetail(Guid id) {
			Role role = _database.FindRoleById(id);
			if (role == null) return new NotFoundResult( );

			List<Guid> topUserIds = new List<Guid>();
			List<string> topUserNames = new List<string>();
			int maxUsersShown = 100;

			for (int i = 0; (i < role.UserRoles.Count && i < maxUsersShown); i++) {
				topUserIds.Add(role.UserRoles[i].User.Id);
				topUserNames.Add(role.UserRoles[i].User.UserName);
			}

			RoleDetailViewModel roleDetailViewModel = new RoleDetailViewModel {
				Id = id,
				Name = role.Name,
				Permissions = role.Permissions.ToString(),
				TopUserIds = topUserIds,
				TopUserNames = topUserNames
			};

			return View(roleDetailViewModel);
		}

		// TODO: this mothod will need to be changed for #68
		[HttpGet]
		public IActionResult Detail(Guid id) {
			User user = _database.FindUserById(id);
			if (user is null) {
				return new NotFoundResult( );
			}
			List<SelectListItem> Roles = new List<SelectListItem>();
			foreach (Role role in _database.GetRoles( )) {
				Roles.Add(new SelectListItem { Value = role.Name, Text = role.Name });
			}
			UserDetailViewModel viewModel = new UserDetailViewModel(user);
			viewModel.Roles = Roles;
			return View(viewModel);
		}

		// TODO: this mothod will need to be changed for #68
		[HttpPost]
		public IActionResult Detail(Guid Id, [FromForm] UserDetailViewModel userDetailViewModel) {
			User user = _database.FindUserById(Id);
			if (user is null) {
				return new NotFoundResult( );
			}
			List<UserRole> userRoles = new List<UserRole>();
			if (userDetailViewModel.SelectedRoles != null)
				foreach (string roleName in userDetailViewModel.SelectedRoles) {
					Role role = _database.GetRoles().FirstOrDefault(r => r.Name == roleName);
					userRoles.Add(new UserRole( ) {
						User = user,
						UserId = user.UserId,
						Role = role,
						RoleId = role.Id
					});
				}
			user.UserRoles = userRoles;
			_database.UpdateUser(user, user.UserId);
			List<SelectListItem> Roles = new List<SelectListItem>();
			foreach (Role role in _database.GetRoles( )) {
				Roles.Add(new SelectListItem { Value = role.Name, Text = role.Name });
			}
			UserDetailViewModel viewModel = new UserDetailViewModel(user);
			viewModel.Roles = Roles;
			return View(viewModel);
		}
	}
}