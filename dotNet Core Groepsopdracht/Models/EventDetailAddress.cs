﻿using dotNet_Core_Groepsopdracht.Data.Database;
using dotNet_Core_Groepsopdracht.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotNet_Core_Groepsopdracht.Models {
	/// <summary>
	/// gives the address of the requested detail view
	/// </summary>
	public class EventDetailAddress {
		
			public Guid Id { get; set; }
			public string Street { get; set; }
			public int Number { get; set; }
			public string Bus { get; set; }
			public string ZipCode { get; set; }
			public string City { get; set; }

	}
}
