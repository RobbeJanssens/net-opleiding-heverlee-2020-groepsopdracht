﻿using dotNet_Core_Groepsopdracht.Entities;
using System;

namespace dotNet_Core_Groepsopdracht.Models {
	public class RoleDeleteViewModel {

		public Guid Id { get; set; }
		public string Name { get; set; }
		public Permissions Permissions { get; set; }
	}
}
