﻿using dotNet_Core_Groepsopdracht.Areas.Identity.Data;
using dotNet_Core_Groepsopdracht.Entities;
using System;
using System.Collections.Generic;

namespace dotNet_Core_Groepsopdracht.Data.Database {

	public interface IDatabase {

		#region Adding

		/// <summary>
		/// Adds address to database
		/// </summary>
		/// <param name="address">Address</param>
		void AddAddress(Address address);

		/// <summary>
		/// Adds event to database
		/// </summary>
		/// <param name="_event">Event</param>
		void AddEvent(Event _event);

		/// <summary>
		/// Adds newsitem to database
		/// </summary>
		/// <param name="newsItem">NewsItem</param>
		void AddNewsItem(NewsItem newsItem);

		/// <summary>
		/// Adds reaction to database
		/// </summary>
		/// <param name="reaction">Reaction</param>
		void AddReaction(Reaction reaction);

		/// <summary>
		/// Adds reservation for logged in user to database
		/// </summary>
		/// <param name="reservation">ReservationLoggedIn</param>
		void AddReservationLoggedIn(ReservationLoggedIn reservation);

		/// <summary>
		/// Adds reservation for non logged in user to database
		/// </summary>
		/// <param name="reservation">ReservationNotLoggedIn</param>
		void AddReservationNotLoggedIn(ReservationNotLoggedIn reservation);

		/// <summary>
		/// Adds role to database
		/// </summary>
		/// <param name="role">Role</param>
		void AddRole(Role role);

		/// <summary>
		/// adds sponsor to database
		/// </summary>
		/// <param name="Sponsor">Sponsor</param>
		void AddSponsor(Sponsor Sponsor);

		/// <summary>
		/// adds user to database(make sure to use user.userId, instead of user.Id!)
		/// </summary>
		/// <param name="user">User</param>
		void AddUser(User user);

		/// <summary>
		/// Adds contactmessage to database
		/// </summary>
		/// <param name="contactmessage">Contactmessage</param>
		void AddContactMessage(Contactmessage contactmessage);

		#endregion Adding

		#region Retrieving single

		/// <summary>
		/// Returns an address based on guid id
		/// </summary>
		/// <param name="id">guid id</param>
		/// <returns> address</returns>
		Address FindAddressById(Guid id);

		/// <summary>
		/// Returns an event based on guid id
		/// </summary>
		/// <param name="id">guid id </param>
		/// <returns>event</returns>
		Event FindEventById(Guid id);

		/// <summary>
		/// Returns an newsitem based on guid id
		/// </summary>
		/// <param name="id">guid id </param>
		/// <returns>newsitem</returns>
		NewsItem FindNewsItemById(Guid id);

		/// <summary>
		/// returns a reaction based on guid id
		/// </summary>
		/// <param name="id">guid id </param>
		/// <returns>reaction</returns>
		Reaction FindReactionById(Guid id);

		/// <summary>
		/// returns a reservation based on guid id
		/// </summary>
		/// <param name="id"> guid id </param>
		/// <returns>logged-in user reservation</returns>
		ReservationLoggedIn FindReservationLoggedInById(Guid id);

		/// <summary>
		/// Returns an reservation made by a non logged-in user based on guid id
		/// </summary>
		/// <param name="id">guid id </param>
		/// <returns>non logged-in user reservation</returns>
		ReservationNotLoggedIn FindReservationNotLoggedInById(Guid id);

		/// <summary>
		/// Returns a role based on guid id
		/// </summary>
		/// <param name="id">guid id </param>
		/// <returns>role</returns>
		Role FindRoleById(Guid id);

		/// <summary>
		/// Returns a sponsor based on guid id
		/// </summary>
		/// <param name="id">guid id </param>
		/// <returns>sponsor</returns>
		Sponsor FindSponsorById(Guid id);

		/// <summary>
		/// Returns a user based on guid id, make sure to use user.userId, instead of user.Id
		/// </summary>
		/// <param name="id"> guid id</param>
		/// <returns>user </returns>
		User FindUserById(Guid id);

		/// <summary>
		/// Returns a contactmessage based on guid id
		/// </summary>
		/// <param name="id">guid id</param>
		/// <returns>contactmessage</returns>
		Contactmessage FindContactMessageById(Guid id);

		#endregion Retrieving single

		#region Retrieving all

		/// <summary>
		/// Returns all addresses currently in database
		/// </summary>
		/// <returns> list of addresses </returns>
		IEnumerable<Address> GetAddresses( );

		/// <summary>
		/// Returns all events currently in database
		/// </summary>
		/// <returns> list of events</returns>
		IEnumerable<Event> GetEvents( );

		/// <summary>
		/// Returns all newsitems currently in database
		/// </summary>
		/// <returns> list of newsitems</returns>
		IEnumerable<NewsItem> GetNewsItems( );

		/// <summary>
		/// Returns all reactions currently in database
		/// </summary>
		/// <returns>list of reactions</returns>
		IEnumerable<Reaction> GetReactions( );

		/// <summary>
		/// Returns all reservations from logged-in users currently in database
		/// </summary>
		/// <returns>list of logged-in users reservations</returns>
		IEnumerable<ReservationLoggedIn> GetReservationsLoggedIn( );

		/// <summary>
		/// Returns all reservation from non logged-in users currently in database
		/// </summary>
		/// <returns>list of non logged-in users reservations</returns>
		IEnumerable<ReservationNotLoggedIn> GetReservationsNotLoggedIn( );

		/// <summary>
		/// Returns all roles currently in database
		/// </summary>
		/// <returns>list of roles</returns>
		IEnumerable<Role> GetRoles( );

		/// <summary>
		/// Returns all sponsors currently in database
		/// </summary>
		/// <returns>list of sponsors</returns>
		IEnumerable<Sponsor> GetSponsors( );

		/// <summary>
		/// Returns all users currently in database
		/// </summary>
		/// <returns>list of users</returns>
		IEnumerable<User> GetUsers( );

		/// <summary>
		/// Returns all contactmessages currently in database
		/// </summary>
		/// <returns>list of contactmessages</returns>
		IEnumerable<Contactmessage> GetContactMessages( );

		#endregion Retrieving all

		#region Removing

		/// <summary>
		/// Removes address from database
		/// </summary>
		/// <param name="address">Address</param>
		void RemoveAddress(Address address);

		/// <summary>
		/// Removes event from database
		/// </summary>
		/// <param name="_event">Event</param>
		void RemoveEvent(Event _event);

		/// <summary>
		/// Removes newsitem from database
		/// </summary>
		/// <param name="newsItem">NewsItem</param>
		void RemoveNewsItem(NewsItem newsItem);

		/// <summary>
		/// Removes reaction from database
		/// </summary>
		/// <param name="reaction">Reaction</param>
		void RemoveReaction(Reaction reaction);

		/// <summary>
		/// Removes logged-in reservation from database
		/// </summary>
		/// <param name="reservation">ReservationLoggedIn</param>
		void RemoveReservationLoggedIn(ReservationLoggedIn reservation);

		/// <summary>
		/// Removes non-logged in reservation from database
		/// </summary>
		/// <param name="reservation">ReservationNotLoggedIn</param>
		void RemoveReservationNotLoggedIn(ReservationNotLoggedIn reservation);

		/// <summary>
		/// Removes role from database
		/// </summary>
		/// <param name="role">Role</param>
		void RemoveRole(Role role);

		/// <summary>
		/// Removes sponsor from database
		/// </summary>
		/// <param name="Sponsor">Sponsor</param>
		void RemoveSponsor(Sponsor Sponsor);

		/// <summary>
		/// Removes user from database
		/// </summary>
		/// <param name="user">User</param>
		void RemoveUser(User user);

		/// <summary>
		/// Removes contactmessage from database
		/// </summary>
		/// <param name="contactmessage">Contactmessage</param>
		void RemoveContactMessage(Contactmessage contactmessage);

		#endregion Removing

		#region Update

		/// <summary>
		/// Updates an existing address
		/// </summary>
		/// <param name="address">Address</param>
		/// <param name="id">guid id</param>
		void UpdateAddress(Address address, Guid id);

		/// <summary>
		/// Updates an existing event
		/// </summary>
		/// <param name="_event">Event</param>
		/// <param name="id">guid id</param>
		void UpdateEvent(Event _event, Guid id);

		/// <summary>
		/// Updates an existing newsitem
		/// </summary>
		/// <param name="newsItem">NewsItem</param>
		/// <param name="id">guid id</param>
		void UpdateNewsItem(NewsItem newsItem, Guid id);

		/// <summary>
		/// Updates an existing reaction
		/// </summary>
		/// <param name="reaction">Reaction</param>
		/// <param name="id">guid id</param>
		void UpdateReaction(Reaction reaction, Guid id);

		/// <summary>
		/// Updates an existing reservation made by a logged-in user
		/// </summary>
		/// <param name="reservation">ReservationLoggedIn</param>
		/// <param name="id">guid id</param>
		void UpdateReservationLoggedIn(ReservationLoggedIn reservation, Guid id);

		/// <summary>
		/// Updates an existing address made by a non logged-in user
		/// </summary>
		/// <param name="reservation">ReservationNotLoggedIn</param>
		/// <param name="id">guid id</param>
		void UpdateReservationNotLoggedIn(ReservationNotLoggedIn reservation, Guid id);

		/// <summary>
		/// Updates an existing role
		/// </summary>
		/// <param name="role">Role</param>
		/// <param name="id">guid id</param>
		void UpdateRole(Role role, Guid id);

		/// <summary>
		/// Updates an existing sponsor
		/// </summary>
		/// <param name="Sponsor">Sponsor</param>
		/// <param name="id">guid id</param>
		void UpdateSponsor(Sponsor Sponsor, Guid id);

		/// <summary>
		/// Updates an existing user(make sure to use user.userId, instead of user.Id!)
		/// </summary>
		/// <param name="user">User</param>
		/// <param name="id">guid id</param>
		void UpdateUser(User user, Guid id);

		/// <summary>
		/// Updates an existing Contactmessage
		/// </summary>
		/// <param name="contactmessage">Contactmessage</param>
		/// <param name="id">guid id</param>
		void UpdateContactMessage(Contactmessage contactmessage, Guid id);

		#endregion Update

		#region Retrieve multiple

		/// <summary>
		/// Returns a list of periods based on the guid EventId
		/// </summary>
		/// <param name="id">guid id </param>
		/// <returns>List of periods</returns>
		List<Period> FindPeriodsByEventId(Guid id);

		/// <summary>
		/// Returns a list of reservations based on the guid UserId
		/// </summary>
		/// <param name="id">guid id </param>
		/// <returns>List of reservations per user</returns>
		List<ReservationLoggedIn> FindReservationsByUserId(Guid id);

		#endregion Retrieve multiple
	}
}