﻿using System;

namespace dotNet_Core_Groepsopdracht.Entities {
	public class Period {


		public Guid Id { get; set; }

		public DateTime Start { get; set; }
		public DateTime End { get; set; }
		public Event Event { get; set; }
		public Guid EventId { get; set; }
	}
}
