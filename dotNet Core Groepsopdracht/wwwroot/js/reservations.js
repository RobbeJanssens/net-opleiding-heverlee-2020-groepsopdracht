﻿
function showTimes(value) {
    var selectedPeriod;
    for (var i = 0; i < periods.length; i++) {
        var period = periods[i];
        if (period[0] == value) {
            selectedPeriod = period;
            break;
        }
    }
    // var selectedDate = new Date(selectedPeriod[1])
    console.log(selectedPeriod);
    var StartDate = new Date(selectedPeriod[5], selectedPeriod[6] - 1, selectedPeriod[7], selectedPeriod[1], selectedPeriod[2]);
    var EndDate = new Date(selectedPeriod[5], selectedPeriod[6] - 1, selectedPeriod[7], selectedPeriod[3], selectedPeriod[4]);
    // Add logic when StartDate equals EndDate;
    var AvailableTimes = new Array();
    var newDate = new Date(StartDate.getTime() - 15 * 60000);
    while (true) {
        newDate = new Date(newDate.getTime() + 15 * 60000);
        if (dates.compare(newDate, EndDate) < 1) {
            AvailableTimes.push(newDate);
            console.log(newDate);
        } else {
            break;
        }
    }
    var ts = document.getElementById("timeSelector");
    if (ts.options.length > 1) {
        // remove any existing options
        for (var i = ts.options.length - 1; i > 0; i--) {
            ts.remove(i);
        }
    }
    console.log("add options");
    console.log(AvailableTimes);
    for (var i = 0; i < AvailableTimes.length; i++) {
        let t = AvailableTimes[i];
        let minutes = String(t.getMinutes());
        if (t.getMinutes() < 10) { minutes = "0" + minutes; }
        let txt = String(t.getHours() + ":" + minutes);
        let val = t.toISOString();
        let newOption = new Option(txt, val);
        ts.options.add(newOption);
    }
    if (AvailableTimes.length > 1) { document.getElementById("timeSelectorDiv").style.display = "block"; }
}

var dates = {
    convert: function (d) {
        return (
            d.constructor === Date ? d :
                d.constructor === Array ? new Date(d[0], d[1], d[2]) :
                    d.constructor === Number ? new Date(d) :
                        d.constructor === String ? new Date(d) :
                            typeof d === "object" ? new Date(d.year, d.month, d.date) :
                                NaN
        );
    },
    compare: function (a, b) {
        // Compare two dates (could be of any type supported by the convert
        // function above) and returns:
        //  -1 : if a < b
        //   0 : if a = b
        //   1 : if a > b
        return (
            isFinite(a = this.convert(a).valueOf()) &&
                isFinite(b = this.convert(b).valueOf()) ?
                (a > b) - (a < b) :
                NaN
        );
    }
}

