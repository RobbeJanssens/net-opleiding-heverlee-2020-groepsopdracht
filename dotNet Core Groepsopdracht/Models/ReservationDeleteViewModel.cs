﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace dotNet_Core_Groepsopdracht.Models {
	public class ReservationDeleteViewModel {

		public Guid Id { get; set; }
		public Guid EventId { get; set; }
		public string EventTitle { get; set; }
		public string Firstname { get; set; }
		public string Lastname { get; set; }

		public int NumberOfPeople { get; set; }
	}
}
