﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace dotNet_Core_Groepsopdracht.Models {

	public class NewsCreateViewModel {
		public Guid Id { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = "Titel is vereist")]
		[DisplayName("Titel*")]
		[StringLength(100, ErrorMessage = "Titel is te lang")]
		public string Title { get; set; }


		[Required(AllowEmptyStrings = false, ErrorMessage = "Inhoud is vereist")]
		[DisplayName("Inhoud*")]
		public string Content { get; set; }


		[DisplayName("Afbeelding*")]
		public IFormFile Image { get; set; }

		//public User Author { get; set; }

		public string Username { get; set; }

	}
}