﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace dotNet_Core_Groepsopdracht.Models {

	public class OverviewEventViewModel {
		// TODO: Re-name clas to be EventIndex/EventDetailViewModel to be consistent??? Or EventOverViewModel.
		// Opgelet: Staat op index.cshtml, maar Action in View verwijst naar Detail controller => not consistent!

		public Guid Id { get; set; }

		[DisplayName("Titel")]
		public String Title { get; set; }

		[DisplayName("Adres")]
		public AddressViewModel Address { get; set; }

		[DisplayName("Evenement periodes")]
		public DateTime[] PeriodeEvenement { get; set; }

		[DisplayName("Beschrijving")]
		public string Description { get; set; }

		[DisplayName("Afbeelding")]
		public string Image { get; set; }

		// TODO: Datatype prijs aanpassen?!
		[DisplayName("Prijs")]
		public string Price { get; set; }

		public List<SponsorViewModel> Sponsors { get; set; }

		public int MaxPlacesLeft { get; set; }
	}
}