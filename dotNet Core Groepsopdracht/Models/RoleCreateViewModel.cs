﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace dotNet_Core_Groepsopdracht.Models {

	public class RoleCreateViewModel {

		[Required(AllowEmptyStrings = false, ErrorMessage = "Naam is vereist")]
		[StringLength(30)]
		[DisplayName("Geef naam voor de nieuwe rol in")]
		public string Name { get; set; }

		public bool RoleManagement { get; set; }
		public bool UserManagement { get; set; }
		public bool CreateNewsItem { get; set; }
		public bool EditNewsItem { get; set; }
		public bool ApproveNewsItem { get; set; }
		public bool EventManagement { get; set; }
		public bool DeleteReaction { get; set; }
		public bool ViewStatistics { get; set; }
		public bool SponsorManagement { get; set; }

	}
}