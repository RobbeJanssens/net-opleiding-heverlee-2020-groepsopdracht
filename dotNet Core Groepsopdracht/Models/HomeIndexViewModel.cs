﻿using System.Collections.Generic;

namespace dotNet_Core_Groepsopdracht.Models {
	public class HomeIndexViewModel {

		public List<HomeIndexEventViewModel> SlideEvents { get; set; }
		public List<HomeIndexEventViewModel> EventsIndex { get; set; }
		public List<HomeIndexNewsitemViewModel> NewsItems { get; set; }
		public string NewsImage { get; set; }

	}
}
