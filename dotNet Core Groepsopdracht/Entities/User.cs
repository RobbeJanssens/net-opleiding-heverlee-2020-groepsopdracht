﻿using dotNet_Core_Groepsopdracht.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.Tracing;

namespace dotNet_Core_Groepsopdracht.Areas.Identity.Data {
	// Add profile data for application users by adding properties to the User class
	public class User : IdentityUser<Guid> {

		//
		// Properties
		//
		public Guid UserId => base.Id; // IdentityUser uses virtual string Id
		public DateTime CreatedOn { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }

		//public string Email { get; set; } //Inherits from IdentityUser

		//public long PhoneNumber { get; set; } //Inherits from IdentityUser
		public Address Address { get; set; }
		public string PhotoUrl { get; set; }

		public List<NewsItem> NewsItems { get; set; }
		public List<Reaction> Reactions { get; set; }
		public List<ReservationLoggedIn> Reservations { get; set; }

		public List<UserRole> UserRoles { get; set; }

		//
		// Methods
		//
		public bool HasPerm(Permissions perm) {
			return GetPermissions( ).HasFlag(perm);
		}

		public Permissions GetPermissions( ) {
			Permissions perms = Permissions.None;
			// Add up all ther permissions of all the roles the user has
			foreach (UserRole ur in UserRoles)
				perms |= ur.Role.Permissions;
			return perms;
		}
	}
}
