﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace dotNet_Core_Groepsopdracht.Models {
	public class SponsorEditViewModel {

		public Guid Id { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = "Naam sponsor is vereist")]
		[DisplayName("Naam*")]
		[StringLength(50, ErrorMessage = "Naam is te lang")]
		public string Naam { get; set; }

		public string Photo { get; set; }

		[DisplayName("Logo*")]

		public IFormFile Image { get; set; }

		[DisplayName("Postcode")]
		[StringLength(4, ErrorMessage = "Postcode is te lang")]
		public string Postcode { get; set; }

		[DisplayName("Link naar website")]
		public string Url { get; set; }

	}
}
