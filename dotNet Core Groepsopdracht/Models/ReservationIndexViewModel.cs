﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotNet_Core_Groepsopdracht.Models {
	public class ReservationIndexViewModel {
		public Guid Id { get; set; }
		public Guid EventId { get; set; }
		public string EventTitle { get; set; }
		public DateTime ReservationDateTime { get; set; }
		public DateTime CreatedDateTime { get; set; }
		public int NumberOfPeople { get; set; }
	}
}
