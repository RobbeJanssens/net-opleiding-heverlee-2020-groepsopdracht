﻿using System;
using System.Collections.Generic;

namespace dotNet_Core_Groepsopdracht.Models {
	public class SponsorViewModel {
		public Guid Id { get; set; }
		public string Naam { get; set; }
		public string Photo { get; set; }

		public string Postcode { get; set; }

		public string Url { get; set; }

		public List<SponsorEventViewModel> Events { get; set; }
	}
}
