﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace dotNet_Core_Groepsopdracht.Models {
	public class PeriodViewModel {

		public Guid Id { get; set; }

		[Required]
		[DisplayName("Startdatum*")]
		[DataType(DataType.DateTime)]
		[Range(typeof(DateTime), "1/1/1900", "1/1/2070")]
		public DateTime Start { get; set; }

		[Required]
		[DisplayName("Einddatum*")]
		[DataType(DataType.DateTime)]
		[Range(typeof(DateTime), "1/1/1900", "1/1/2070")]
		public DateTime End { get; set; }

		public bool RemoveRemoveButton { get; set; }
	}
}