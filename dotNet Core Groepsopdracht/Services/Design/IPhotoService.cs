﻿using Microsoft.AspNetCore.Http;

namespace dotNet_Core_Groepsopdracht.Services.Design {
	public interface IPhotoService {

		void DeletePicture(string photoUrl);

		string UploadedFile(IFormFile photo);
		string UploadedFile(IFormFile photo, long maxFileSize);
	}
}
