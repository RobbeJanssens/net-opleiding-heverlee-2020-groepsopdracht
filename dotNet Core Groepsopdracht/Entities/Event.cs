﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace dotNet_Core_Groepsopdracht.Entities {
	public class Event {

		public Guid Id { get; set; }
		public String Title { get; set; }
		public int MaxCap { get; set; }
		public int MinCap { get; set; }
		public Address Address { get; set; }
		public List<Period> Periods { get; set; }
		public string Description { get; set; }
		public string Image { get; set; }
		public string Price { get; set; }
		public List<SponsorEvent> SponsorEvents { get; set; }

		public List<ReservationLoggedIn> ReservationsLoggedIn { get; set; }
		public List<ReservationNotLoggedIn> ReservationsNotLoggedIn { get; set; }

		public int MaxPlacesLeft( ) {
			int max = 0;
			foreach (Period period in Periods) {
				int currentPlacesLeft = MaxCap;
				if (ReservationsLoggedIn != null)
					foreach (ReservationLoggedIn reservation in ReservationsLoggedIn.Where(r => r.ReservationDateTime <= period.End && r.ReservationDateTime >= period.Start)) {
						currentPlacesLeft -= reservation.NumberOfPeople;
					}
				if (ReservationsNotLoggedIn != null)
					foreach (ReservationNotLoggedIn reservation in ReservationsNotLoggedIn.Where(r => r.ReservationDateTime <= period.End && r.ReservationDateTime >= period.Start)) {
						currentPlacesLeft -= reservation.NumberOfPeople;
					}
				if (currentPlacesLeft > max) { max = currentPlacesLeft; }
			}
			return max;
		}
	}
}