﻿using dotNet_Core_Groepsopdracht.Areas.Identity.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace dotNet_Core_Groepsopdracht.Models {
	public class UserListViewModel {
		public Guid UserId { get; set; }
		public string UserName { get; set; }

		[DisplayName("Voornaam")]
		public string Firstname { get; set; }

		[DisplayName("Achternaam")]
		public string Lastname { get; set; }
		public string Email { get; set; }

		[DisplayName("Aangemaakt op")]
		public DateTime CreatedOn { get; set; }
		public string PhoneNumber { get; set; }
		public string[] SelectedRoles { get; set; }
		public List<SelectListItem> Roles { get; set; }

		public string Image { get; set; }


		public UserListViewModel( ) { }

		public UserListViewModel(User user) {
			UserId = user.UserId;
			UserName = user.UserName;
			Firstname = user.FirstName;
			Lastname = user.LastName;
			Email = user.Email;
			CreatedOn = user.CreatedOn;
			PhoneNumber = user.PhoneNumber;
			Image = user.PhotoUrl;

			SelectedRoles = new string[user.UserRoles.Count];
			for (int i = 0; i < user.UserRoles.Count; i++) {
				SelectedRoles[i] = (user.UserRoles[i].Role.Name);
			}
		}

	}
}
