﻿using dotNet_Core_Groepsopdracht.Data.Database;
using dotNet_Core_Groepsopdracht.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace dotNet_Core_Groepsopdracht.Models {
	public class EventDetailViewModel {

		public Guid Id { get; set; }

		public String Title { get; set; }

		[DisplayName("Maximum capaciteit")]
		public int MaxCap { get; set; }

		[DisplayName("Minimum capaciteit")]
		public int MinCap { get; set; }

		[DisplayName("Beschrijving")]
		public string Description { get; set; }

		[DisplayName("Afbeelding")]
		public string Image { get; set; }

		[DisplayName("Prijs")]
		public string Price { get; set; }

		//address
		public string Street { get; set; }

		public int Number { get; set; }

		public string Bus { get; set; }

		public string ZipCode { get; set; }

		public string City { get; set; }

		//period

		public List<EventPeriodViewModel> Periods = new List<EventPeriodViewModel>();
		public List<SponsorViewModel> Sponsors = new List<SponsorViewModel>();



	}



}
