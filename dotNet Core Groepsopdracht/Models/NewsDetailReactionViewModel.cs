﻿using System;
using System.ComponentModel;

namespace dotNet_Core_Groepsopdracht.Models {

	public class NewsDetailReactionViewModel {
		public Guid Id { get; set; }
		public Guid AuthorId { get; set; }

		[DisplayName("Titel")]
		public string Title { get; set; }

		[DisplayName("Datum aangemaakt")]
		public DateTime DateMade { get; set; }

		[DisplayName("Auteur")]
		public string AuthorUserName { get; set; }

		[DisplayName("Inhoud")]
		public string Content { get; set; }
	}
}