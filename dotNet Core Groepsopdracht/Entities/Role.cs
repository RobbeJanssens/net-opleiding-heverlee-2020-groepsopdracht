﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace dotNet_Core_Groepsopdracht.Entities {
	public class Role : IdentityRole<Guid> {
		//
		// Fields
		//

		//
		// Constructors
		//

		//
		// Properties
		////
		public List<UserRole> UserRoles { get; set; }
		public override Guid Id {
			get => base.Id;
			set => base.Id = value;
		}
		public override string Name {
			get => base.Name;
			set => base.Name = value;
		}
		public Permissions Permissions { get; set; }

		//
		// Methodes
		//
		public bool HasPerm(Permissions perm) {
			return Permissions.HasFlag(perm);
		}
	}


	[Flags]
	public enum Permissions : ulong {
		/// <summary> Empty permission </summary>
		None = 0,
		/// <summary> Allows user to create, edit, and delete a role </summary>
		RoleManagement = 1 << 0,
		/// <summary> Allows user to manage users, and edit their roles </summary>
		UserManagement = 1 << 1,
		/// <summary> Allows user to create new articles </summary>
		CreateNewsItem = 1 << 2,
		/// <summary> Allows user to edit other's articles </summary>
		EditNewsItem = 1 << 3,
		/// <summary> Allows approving of articles or deleting articles </summary>
		ApproveNewsItem = 1 << 4,
		/// <summary> Allows Creating, editing and deleting of events </summary>
		EventManagement = 1 << 5,
		/// <summary> Allows deleting of other user's reactions </summary>
		DeleteReaction = 1 << 6,
		/// <summary> Allows user to view statistic </summary>
		ViewStatistics = 1 << 7,
		/// <summary> Allows for adding, editing and deleting sponsors </summary>
		SponsorManagement = 1 << 8
	}

	// email: Test0@test.com
	// pass: Test0@test.com
}
