﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace dotNet_Core_Groepsopdracht.Models {

	public class NewsDetailViewModel {
		public Guid Id { get; set; }


		public string Title { get; set; }

		[DisplayName("Datum aangemaakt")]
		public DateTime DateCreated { get; set; }

		[DisplayName("Datum aangepast")]
		public DateTime? DateAdjusted { get; set; }

		[DisplayName("Inhoud")]
		public string Content { get; set; }

		[DisplayName("Afbeelding URL")]
		public string ImageUrl { get; set; }

		[DisplayName("Auteur")]
		public string AuthorUserName { get; set; }

		public Guid AuthorId { get; set; }

		// TODO: add author's img
		[DisplayName("Reacties")]
		public List<NewsDetailReactionViewModel> Reactions { get; set; }

		[DisplayName("Titel")]
		[Required]
		public string ReactionTitle { get; set; }

		[DisplayName("Reactie")]
		[Required]
		public string Reaction { get; set; }


	}
}