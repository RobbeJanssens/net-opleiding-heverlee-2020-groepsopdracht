﻿using dotNet_Core_Groepsopdracht.Entities;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace dotNet_Core_Groepsopdracht.Models {

	public class RoleEditViewModel {
		public Guid Id { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = "Naam is vereist.")]
		[StringLength(30)]
		[DisplayName("Benaming rol")]
		public string Name { get; set; }

		// TODO: To be added in the view?!
		public Permissions Permissions { get; set; }

		public bool RoleManagement { get; set; }
		public bool UserManagement { get; set; }
		public bool CreateNewsItem { get; set; }
		public bool EditNewsItem { get; set; }
		public bool ApproveNewsItem { get; set; }
		public bool EventManagement { get; set; }
		public bool DeleteReaction { get; set; }
		public bool ViewStatistics { get; set; }
		public bool SponsorManagement { get; set; }

	}
}