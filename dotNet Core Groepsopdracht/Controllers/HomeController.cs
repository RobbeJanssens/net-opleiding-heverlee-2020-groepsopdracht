using dotNet_Core_Groepsopdracht.Areas.Identity.Data;
using dotNet_Core_Groepsopdracht.Data.Database;
using dotNet_Core_Groepsopdracht.Entities;
using dotNet_Core_Groepsopdracht.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace dotNet_Core_Groepsopdracht.Controllers {


	public class HomeController : Controller {
		private readonly ILogger<HomeController> _logger;
		private readonly UserManager<User> _userManager;
		private readonly IDatabase _database;

		public HomeController(ILogger<HomeController> logger,
			UserManager<User> userManager, IDatabase database) {
			_logger = logger;
			_userManager = userManager;
			_database = database;
		}

		public IActionResult Index( ) {
			Random randgen = new Random();
			// Get all events and sort them by the starting datetime of the first period.
			List<Event> allEvents = _database.GetEvents().OrderBy(e => e.Periods[0].Start).ToList();
			List<NewsItem> allNewsItems = _database.GetNewsItems().OrderBy(n => n.DateCreated).ToList();
			List<HomeIndexEventViewModel> slideEvents = new List<HomeIndexEventViewModel>();
			int maxSlides = allEvents.Count < 3 ? allEvents.Count : 3;
			for (int i = 0; i < maxSlides; i++) {
				string eventImage = "/images/placeholder.jpg";
				if (allEvents[i].Image != null) eventImage = allEvents[i].Image;
				slideEvents.Add(new HomeIndexEventViewModel {
					EventId = allEvents[i].Id,
					Title = allEvents[i].Title,
					Description = allEvents[i].Description,
					EventImage = eventImage
				});
			}
			List<HomeIndexEventViewModel> eventsIndex = new List<HomeIndexEventViewModel>();
			int maxEvents = allEvents.Count < 6 ? allEvents.Count : 6;
			for (int i = allEvents.Count - 1; i >= allEvents.Count - maxEvents; i--) {
				string eventImage = "/images/placeholder.jpg";
				if (allEvents[i].Image != null) eventImage = allEvents[i].Image;
				eventsIndex.Add(new HomeIndexEventViewModel {
					EventId = allEvents[i].Id,
					Title = allEvents[i].Title,
					Description = allEvents[i].Description,
					EventImage = eventImage
				});
			}
			List<HomeIndexNewsitemViewModel> newsItems = new List<HomeIndexNewsitemViewModel>();
			int maxNewsItems = allNewsItems.Count < 4 ? allNewsItems.Count : 4;
			for (int i = allNewsItems.Count - 1; i >= allNewsItems.Count - maxNewsItems; i--) {
				newsItems.Add(new HomeIndexNewsitemViewModel {
					NewsItemId = allNewsItems[i].Id,
					Title = allNewsItems[i].Title,
					ImageUrl = allNewsItems[i].ImageUrl
				});
			}
			string newsImage = null;
			bool found = false;
			while (found == false) {
				newsImage = allNewsItems[randgen.Next(allNewsItems.Count)].ImageUrl;
				if (newsImage != null) found = true;
			}
			HomeIndexViewModel viewModel = new HomeIndexViewModel{
				SlideEvents = slideEvents,
				EventsIndex = eventsIndex,
				NewsItems = newsItems,
				NewsImage = newsImage
			};

			return View(viewModel);
		}

		public IActionResult Privacy( ) {
			return View( );
		}

		public IActionResult ContactOverview( ) {
			//omzetten van Contacts naar ContactOverviewViewModel-lijst:
			List<ContactOverviewViewModel> covmList = new List<ContactOverviewViewModel>();
			foreach (Entities.Contactmessage contactmessage in _database.GetContactMessages( )) {
				covmList.Add(
					new ContactOverviewViewModel {
						Id = contactmessage.Id,
						Name = contactmessage.Name,
						Email = contactmessage.Email,
						MessageContent = contactmessage.MessageContent,
					}
				);
			}

			return View(covmList);
		}

		[HttpGet]
		public async Task<IActionResult> Contact( ) {
			ContactViewModel contactViewModel = new ContactViewModel();
			if (User.Identity.IsAuthenticated) {
				User user = await _userManager.GetUserAsync(User);
				contactViewModel.Name = user.FirstName + " " + user.LastName;
				contactViewModel.Email = user.Email;
			}
			return View(contactViewModel);
		}

		[HttpPost]
		public IActionResult Contact([FromForm] ContactViewModel viewModel) {
			if (!TryValidateModel(viewModel))
				return View(viewModel);
			Contactmessage newcontactmessage = new Contactmessage{
				Id= Guid.NewGuid(),
				Email=viewModel.Email,
				MessageContent=viewModel.Message,
				Name=viewModel.Name
			};
			_database.AddContactMessage(newcontactmessage);
			return RedirectToAction(nameof(ContactConfirm));
		}
		[HttpGet]
		public IActionResult ContactConfirm( ) {
			return View( );
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error( ) {
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}
	}
}