using dotNet_Core_Groepsopdracht.Areas.Identity.Data;
using dotNet_Core_Groepsopdracht.Data.Database;
using dotNet_Core_Groepsopdracht.Database;
using dotNet_Core_Groepsopdracht.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace dotNet_Core_Groepsopdracht.Services {

	public class Database : IDatabase {
		public DatabaseContext _dbContext;

		public Database(DatabaseContext dbContext) {
			_dbContext = dbContext;
		}

		#region Add

		public void AddAddress(Address address) {
			_dbContext.Addresses.Add(address);
			_dbContext.SaveChanges( );
		}

		public void AddEvent(Event ivent) {
			_dbContext.Events.Add(ivent);
			_dbContext.SaveChanges( );
		}

		public void AddNewsItem(NewsItem newsItem) {
			_dbContext.NewsItems.Add(newsItem);
			_dbContext.SaveChanges( );
		}

		public void AddReaction(Reaction reaction) {
			_dbContext.Reactions.Add(reaction);
			_dbContext.SaveChanges( );
		}

		public void AddReservationLoggedIn(ReservationLoggedIn reservation) {
			_dbContext.ReservationsLoggedIn.Add(reservation);
			_dbContext.SaveChanges( );
		}

		public void AddReservationNotLoggedIn(ReservationNotLoggedIn reservation) {
			_dbContext.ReservationsNotLoggedIn.Add(reservation);
			_dbContext.SaveChanges( );
		}

		public void AddRole(Role role) {
			_dbContext.Roles.Add(role);
			_dbContext.SaveChanges( );
		}

		public void AddSponsor(Sponsor Sponsor) {
			_dbContext.Sponsors.Add(Sponsor);
			_dbContext.SaveChanges( );
		}

		public void AddUser(User user) {
			_dbContext.Users.Add(user);
			_dbContext.SaveChanges( );
		}

		public void AddContactMessage(Contactmessage contactmessage) {
			_dbContext.ContactMessages.Add(contactmessage);
			_dbContext.SaveChanges( );
		}

		#endregion Add

		#region FindById

		public Address FindAddressById(Guid id) {
			return _dbContext.Addresses.FirstOrDefault(a => a.Id == id);
		}

		public Event FindEventById(Guid id) {
			return _dbContext.Events.Include("ReservationsLoggedIn.User").Include("Periods").Include("Address").Include("ReservationsNotLoggedIn").Include("SponsorEvents.Sponsor")
				.FirstOrDefault(e => e.Id == id);
		}

		public NewsItem FindNewsItemById(Guid id) {
			return _dbContext.NewsItems.Include("Reactions.Author").Include("Author").FirstOrDefault(n => n.Id == id);
		}

		public Reaction FindReactionById(Guid id) {
			return _dbContext.Reactions.FirstOrDefault(r => r.Id == id);
		}

		public ReservationLoggedIn FindReservationLoggedInById(Guid id) {
			return _dbContext.ReservationsLoggedIn.Include("User").Include("Event").FirstOrDefault(r => r.Id == id);

		}

		public ReservationNotLoggedIn FindReservationNotLoggedInById(Guid id) {
			return _dbContext.ReservationsNotLoggedIn.FirstOrDefault(r => r.Id == id);
		}

		public Role FindRoleById(Guid id) {
			return _dbContext.Roles.Include("UserRoles.User").FirstOrDefault(r => r.Id == id);
		}

		public Sponsor FindSponsorById(Guid id) {
			return _dbContext.Sponsors.Include("SponsorEvents.Event").FirstOrDefault(s => s.Id == id);
		}

		public User FindUserById(Guid id) {
			return _dbContext.Users
				.Include(u => u.Address)
				.Include(u => u.NewsItems)
				.Include("Reservations.Event")
				.Include("UserRoles.Role")
				.AsEnumerable( ).FirstOrDefault(u => u.UserId == id);
		}

		public Contactmessage FindContactMessageById(Guid id) {
			return _dbContext.ContactMessages.FirstOrDefault(e => e.Id == id);
		}

		#endregion FindById

		#region Get

		public IEnumerable<Address> GetAddresses( ) {
			return _dbContext.Addresses.AsEnumerable( );
		}

		public IEnumerable<Event> GetEvents( ) {
			return _dbContext.Events.Include(e => e.Address).Include(e => e.Periods).Include(e => e.SponsorEvents).Include(e => e.ReservationsLoggedIn).Include(e => e.ReservationsNotLoggedIn).AsEnumerable( );
		}

		public IEnumerable<NewsItem> GetNewsItems( ) {
			return _dbContext.NewsItems.Include("Reactions").AsEnumerable( );
		}

		public IEnumerable<Reaction> GetReactions( ) {
			return _dbContext.Reactions.AsEnumerable( );
		}

		public IEnumerable<ReservationLoggedIn> GetReservationsLoggedIn( ) {
			return _dbContext.ReservationsLoggedIn.AsEnumerable( );
		}

		public IEnumerable<ReservationNotLoggedIn> GetReservationsNotLoggedIn( ) {
			return _dbContext.ReservationsNotLoggedIn.AsEnumerable( );
		}

		public IEnumerable<Role> GetRoles( ) {
			return _dbContext.Roles.Include("UserRoles.User").AsEnumerable( );
		}

		public IEnumerable<Sponsor> GetSponsors( ) {
			return _dbContext.Sponsors.AsEnumerable( );
		}

		public IEnumerable<User> GetUsers( ) {
			return _dbContext.Users.Include("UserRoles.Role").AsEnumerable( );
		}

		public IEnumerable<Contactmessage> GetContactMessages( ) {
			return _dbContext.ContactMessages.AsEnumerable( );
		}

		#endregion Get

		#region Remove

		public void RemoveAddress(Address address) {
			_dbContext.Addresses.Remove(address);
			_dbContext.SaveChanges( );
		}

		public void RemoveEvent(Event _event) {
			_dbContext.Events.Remove(_event);
			_dbContext.SaveChanges( );
		}

		public void RemoveNewsItem(NewsItem newsItem) {
			_dbContext.NewsItems.Remove(newsItem);
			_dbContext.SaveChanges( );
		}

		public void RemoveReaction(Reaction reaction) {
			_dbContext.Reactions.Remove(reaction);
			_dbContext.SaveChanges( );
		}

		public void RemoveReservationLoggedIn(ReservationLoggedIn reservation) {
			_dbContext.ReservationsLoggedIn.Remove(reservation);
			_dbContext.SaveChanges( );
		}

		public void RemoveReservationNotLoggedIn(ReservationNotLoggedIn reservation) {
			_dbContext.ReservationsNotLoggedIn.Remove(reservation);
			_dbContext.SaveChanges( );
		}

		public void RemoveRole(Role role) {
			_dbContext.Roles.Remove(role);
			_dbContext.SaveChanges( );
		}

		public void RemoveSponsor(Sponsor sponsor) {
			_dbContext.Sponsors.Remove(sponsor);
			_dbContext.SaveChanges( );
		}

		public void RemoveUser(User user) {
			_dbContext.Users.Remove(user);
			_dbContext.SaveChanges( );
		}

		public void RemoveContactMessage(Contactmessage contactmessage) {
			_dbContext.ContactMessages.Remove(contactmessage);
			_dbContext.SaveChanges( );
		}

		#endregion Remove

		#region Update

		public void UpdateAddress(Address address, Guid id) {
			Address addressEntity = FindAddressById(id);

			if (!(addressEntity is null)) {
				addressEntity.Street = address.Street;
				addressEntity.Number = address.Number;
				addressEntity.ZipCode = address.ZipCode;
				addressEntity.City = address.City;
			}
			_dbContext.SaveChanges( );
		}

		public void UpdateEvent(Event _event, Guid id) {
			Event eventsEntitie = FindEventById(id);

			if (!(eventsEntitie is null)) {
				eventsEntitie.Title = _event.Title;
				eventsEntitie.MaxCap = _event.MaxCap;
				eventsEntitie.MinCap = _event.MinCap;
				eventsEntitie.Address = _event.Address;
				eventsEntitie.Periods = _event.Periods;
				eventsEntitie.Description = _event.Description;
				eventsEntitie.Image = _event.Image;
				eventsEntitie.Price = _event.Price;
				eventsEntitie.SponsorEvents = _event.SponsorEvents;
				eventsEntitie.ReservationsLoggedIn = _event.ReservationsLoggedIn;
				eventsEntitie.ReservationsNotLoggedIn = _event.ReservationsNotLoggedIn;

			}
			_dbContext.SaveChanges( );
		}

		public void UpdateNewsItem(NewsItem newsItem, Guid id) {
			NewsItem newsItemEntitie = FindNewsItemById(id);

			if (!(newsItemEntitie is null)) {
				newsItemEntitie.Title = newsItem.Title;
				newsItemEntitie.DateCreated = newsItem.DateCreated;
				newsItemEntitie.DateAdjusted = newsItem.DateAdjusted;
				newsItemEntitie.Content = newsItem.Content;
				newsItemEntitie.Approved = newsItem.Approved;
				newsItemEntitie.ImageUrl = newsItem.ImageUrl;
				newsItemEntitie.Author = newsItem.Author;
				newsItemEntitie.Reactions = newsItem.Reactions;
			}
			_dbContext.SaveChanges( );
		}

		public void UpdateReaction(Reaction reaction, Guid id) {
			Reaction reactionEntitie = FindReactionById(id);

			if (!(reactionEntitie is null)) {
				reactionEntitie.Title = reaction.Title;
				reactionEntitie.DateMade = reaction.DateMade;
				reactionEntitie.Author = reaction.Author;
				reactionEntitie.Content = reaction.Content;
				reactionEntitie.NewsItem = reaction.NewsItem;
			}
			_dbContext.SaveChanges( );
		}

		public void UpdateReservationLoggedIn(ReservationLoggedIn reservation, Guid id) {
			ReservationLoggedIn reservationLoggedIn = FindReservationLoggedInById(id);

			if (!(reservationLoggedIn is null)) {
				reservationLoggedIn.User = reservation.User;
			}
			_dbContext.SaveChanges( );
		}

		public void UpdateReservationNotLoggedIn(ReservationNotLoggedIn reservation, Guid id) {
			ReservationNotLoggedIn ReservationNotLoggedInEntitie = FindReservationNotLoggedInById(id);

			if (!(ReservationNotLoggedInEntitie is null)) {
				ReservationNotLoggedInEntitie.FirstName = reservation.FirstName;
				ReservationNotLoggedInEntitie.LastName = reservation.LastName;
				ReservationNotLoggedInEntitie.Email = reservation.Email;
				ReservationNotLoggedInEntitie.PhoneNumber = reservation.PhoneNumber;
				ReservationNotLoggedInEntitie.Zipcode = reservation.Zipcode;
			}
			_dbContext.SaveChanges( );
		}

		public void UpdateRole(Role role, Guid id) {
			Role roleEntitie = FindRoleById(id);

			if (!(roleEntitie is null)) {
				roleEntitie.Name = role.Name;
				roleEntitie.Permissions = role.Permissions;
			}
			_dbContext.SaveChanges( );
		}

		public void UpdateSponsor(Sponsor sponsor, Guid id) {
			Sponsor sponsorEntitie = FindSponsorById(id);

			if (!(sponsorEntitie is null)) {
				sponsorEntitie.Naam = sponsor.Naam;
				sponsorEntitie.Photo = sponsor.Photo;
				sponsorEntitie.Postcode = sponsor.Postcode;
				sponsorEntitie.Url = sponsor.Url;
				sponsorEntitie.SponsorEvents = sponsor.SponsorEvents;
			}
			_dbContext.SaveChanges( );
		}

		public void UpdateUser(User user, Guid id) {
			User userEntitie = FindUserById(id);

			if (!(userEntitie is null)) {
				userEntitie.CreatedOn = user.CreatedOn;
				userEntitie.FirstName = user.FirstName;
				userEntitie.LastName = user.LastName;
				userEntitie.Address = user.Address;
				userEntitie.PhotoUrl = user.PhotoUrl;
				userEntitie.NewsItems = user.NewsItems;
				userEntitie.Reactions = user.Reactions;
				userEntitie.Reservations = user.Reservations;
				userEntitie.PhoneNumber = user.PhoneNumber;
				userEntitie.Email = user.Email;
				userEntitie.UserName = user.UserName;
				userEntitie.LockoutEnabled = user.LockoutEnabled;
				userEntitie.LockoutEnd = user.LockoutEnd;
			}
			_dbContext.SaveChanges( );
		}

		public void UpdateContactMessage(Contactmessage contactmessage, Guid id) {
			Contactmessage contactmessageEntitie = FindContactMessageById(id);

			if (!(contactmessageEntitie is null)) {
				contactmessageEntitie.Name = contactmessage.Name;
				contactmessageEntitie.Email = contactmessage.Email;
				contactmessageEntitie.MessageContent = contactmessage.MessageContent;
			}
			_dbContext.SaveChanges( );
		}

		#endregion Update

		#region Period

		public List<Period> FindPeriodsByEventId(Guid id) {
			return _dbContext.Periods.Where(p => p.EventId == id).ToList( );
		}

		#endregion Period

		#region Reservation

		public List<ReservationLoggedIn> FindReservationsByUserId(Guid id) {
			return _dbContext.ReservationsLoggedIn.Where(r => r.UserId == id).ToList( );
		}

		#endregion Reservation
	}
}
