﻿using dotNet_Core_Groepsopdracht.Services.Design;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;

namespace dotNet_Core_Groepsopdracht.Services {
	public class PhotoService : IPhotoService {
		private readonly IWebHostEnvironment _webHostEnvironment;

		public PhotoService(IWebHostEnvironment webHostEnvironment) {
			_webHostEnvironment = webHostEnvironment;
		}

		public void DeletePicture(string photoUrl) {
			if (photoUrl.StartsWith("/")) photoUrl = photoUrl.Substring(1);

			string pathName = Path.Combine(_webHostEnvironment.WebRootPath, photoUrl);
			File.Delete(pathName);
		}

		public string UploadedFile(IFormFile iFormFile, long maxFileSize) {

			if (iFormFile != null) {
				if (iFormFile.Length > maxFileSize) {
					throw new Exception("Bestand " + iFormFile.FileName + " is te groot, maximum " + (maxFileSize / 1048576).ToString("0.0") + " MB.");
				}
			}
			return UploadedFile(iFormFile);
		}
		public string UploadedFile(IFormFile iFormFile) {
			string uniqueFileName = null;
			string filePath = null;

			if (iFormFile != null) {
				string uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "images");
				uniqueFileName = Guid.NewGuid( ).ToString( ) + "_" + iFormFile.FileName;
				filePath = Path.Combine(uploadsFolder, uniqueFileName);
				using (FileStream fileStream = new FileStream(filePath, FileMode.Create)) {
					iFormFile.CopyTo(fileStream);
				}
			}
			return "/images/" + uniqueFileName;
		}
	}
}
